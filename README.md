# Alfred Qiita Workflow

![alt tag](https://raw.github.com/oame/alfred-qiita-workflow/master/screenshots/qiita-workflow.png)

## Installation

Double-click `Qiita.alfredworkflow` to install workflow.

## Development installation

```
$ bundle install --path vendor/bundle --binstubs=.bundle/bin
$ bundle exec rake link
```

If you using Alfred with Dropbox Sync folder, you should type `rake install` instead of `rake link`.

## Thanks

- [Alfred 2 Ruby Template](https://github.com/zhaocai/alfred2-ruby-template)
